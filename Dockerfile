FROM mcr.microsoft.com/dotnet/core/sdk:3.1

RUN apt-get update
RUN apt-get install net-tools
RUN netstat -ntlap
RUN ifconfig
RUN curl -v 172.17.0.1:10000
RUN curl -v 172.17.0.1:10000/devstoreaccount1
RUN curl -v 172.17.0.1:10001
RUN curl -v 172.17.0.1:10001/devstoreaccount1
